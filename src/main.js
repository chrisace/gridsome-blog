// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api
import '~/assets/scss/main.scss'
// import '~/assets/css/style.css'
import DefaultLayout from '~/layouts/Default.vue'
// import 'prismjs/themes/prism-tomorrow.css'
require('typeface-work-sans')
require('typeface-raleway')

export default function (Vue, { router, head, isClient }) {
  // Set default layout as a global component
  Vue.component('Layout', DefaultLayout)
  head.meta.push({
    name: 'robots',
    content: 'noindex'
  })
}
