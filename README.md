# Personal Blog

## Intro

This is a side project to help strengthen my skills with Gridsome and Vue. The purpose of this project is to setup a personal blog so that I can start blogging all my thoughts here. Although I will connect to a headless CMS for fun and learning, I primarily want the blog to be in markdowon that I write in VSCODE.


## Goals
- Continue to familiarize with Gridsome and GraphQL
- Connect Gridsome to a headless CMS (Forestry)


## Tech Used

- Vue / Javascript
- GraphQL
- HTML
- CSS
- Figma for mockups

## Services Used

- [Cloudinary](https://cloudinary.com/) -- Image Hosting
- [GitLab](https://gitlab.com/) -- Git Repo
- [Netlify](https://netlify.com/) -- Static File Hosting and deployment
- [Forestry](https://forestry.io/) -- Git Based headless CMS
